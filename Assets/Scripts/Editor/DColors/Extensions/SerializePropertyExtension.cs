﻿using System;
using UnityEditor;

namespace DColors.Extensions {
	public static class SerializePropertyExtension {
		public static void ForEach(this SerializedProperty array, Action<SerializedProperty> action) {
			for (var index = 0; index < array.arraySize; ++index) {
				var arrayElementAtIndex = array.GetArrayElementAtIndex(index);
				action(arrayElementAtIndex);
			}
		}

		public static void ForEach(this SerializedProperty array, Action<SerializedProperty, int> action) {
			for (var index = 0; index < array.arraySize; ++index) {
				var element = array.GetArrayElementAtIndex(index);
				action(element, index);
			}
		}

		public static void ForEach(this SerializedProperty array, Action<SerializedProperty, int, int> action) {
			for (var index = 0; index < array.arraySize; ++index) {
				var arrayElementAtIndex = array.GetArrayElementAtIndex(index);
				action(arrayElementAtIndex, index, array.arraySize);
			}
		}

		public static SerializedProperty Find(this SerializedProperty array,
			Predicate<SerializedProperty> match) {
			for (var index = 0; index < array.arraySize; ++index) {
				var element = array.GetArrayElementAtIndex(index);
				if (match.Invoke(element))
					return element;
			}
			return null;
		}

		public static void Remove(this SerializedProperty array,
			Predicate<SerializedProperty> match) {
			for (var index = 0; index < array.arraySize; ++index) {
				var element = array.GetArrayElementAtIndex(index);
				if (!match.Invoke(element))
					continue;
				array.DeleteArrayElementAtIndex(index);
				return;
			}
		}
	}
}
