﻿using System;
using System.Collections.Generic;
using System.IO;
using DColors.Db;
using DColors.Extensions;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DColors {
	[InitializeOnLoad]
	public class EditorDebugClassColors : EditorWindow {
		private const string NameProperty = "Name";
		private const string ColorProperty = "Color";
		private const string IsLogProperty = "IsLogEnabled";
		private const string UseColorProperty = "UseColor";
		private static SerializedObject _serializedObject;
		private static SerializedProperty _array;
		private static EditorDebugClassColors _window;
		private Vector2 _position;

		static EditorDebugClassColors() { AssemblyReloadEvents.afterAssemblyReload += OnAssemblyReload; }

		private static void OnAssemblyReload() {
			AssemblyReloadEvents.afterAssemblyReload -= OnAssemblyReload;
			LoadClassColorBase();
			if (_serializedObject == null)
				return;
			SearchAssetScripts();
			_serializedObject.ApplyModifiedProperties();
		}

		[MenuItem("Tools/DebugColors")]
		private static void Init() {
			_window = (EditorDebugClassColors) GetWindow(typeof(EditorDebugClassColors), false, "DColors");
			_window.Show();
		}

		private void OnInspectorUpdate() { LoadClassColorBase(); }

		private static void LoadClassColorBase() {
			var obj = Resources.Load<DColorsBase>("DColorsBase");
			if (obj == null)
				return;
			_serializedObject = new SerializedObject(obj);
			_array = _serializedObject.FindProperty("_array");
		}

		private void OnGUI() {
			if (_serializedObject == null)
				return;

			_position = EditorGUILayout.BeginScrollView(_position, false, false);
			EditorGUI.BeginChangeCheck();
			if (GUILayout.Button("Randomize colors"))
				RandomizeColors();
			GUILayout.Space(10f);
			var property = _serializedObject.FindProperty("_isLogOther");
			property.boolValue = EditorGUILayout.Toggle("Is Log Other", property.boolValue);
			GUILayout.Space(10f);
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Enable Colors All"))
				_array.ForEach(e => e.FindPropertyRelative("UseColor").boolValue = true);
			if (GUILayout.Button("Disable Colors All"))
				_array.ForEach(e => e.FindPropertyRelative("UseColor").boolValue = false);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Enable Log All"))
				_array.ForEach(e => e.FindPropertyRelative("IsLogEnabled").boolValue = true);
			if (GUILayout.Button("Disable Log All"))
				_array.ForEach(e => e.FindPropertyRelative("IsLogEnabled").boolValue = false);
			EditorGUILayout.EndHorizontal();
			DrawGUI();
			if (EditorGUI.EndChangeCheck())
				_serializedObject.ApplyModifiedProperties();
			EditorGUILayout.EndScrollView();
		}

		private void RandomizeColors() {
			_array.ForEach((element, index) => {
				var color = element.FindPropertyRelative("Color");
				color.colorValue = Random.ColorHSV(0.0f, 1f, 0.0f, 1f, 0.0f, 1f, 1f, 1f);
			});
		}

		private void DrawGUI() {
			_array.ForEach((element, index) => {
				var name = element.FindPropertyRelative("Name");
				var color = element.FindPropertyRelative("Color");
				var useColor = element.FindPropertyRelative("UseColor");
				var isLogEnabled = element.FindPropertyRelative("IsLogEnabled");
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField(name.stringValue, GUILayout.Width(170f));
				color.colorValue = EditorGUILayout.ColorField(color.colorValue);
				GUILayout.Space(10f);
				useColor.boolValue =
					EditorGUILayout.ToggleLeft("Color", useColor.boolValue, GUILayout.Width(50f));
				isLogEnabled.boolValue =
					EditorGUILayout.ToggleLeft("IsLog", isLogEnabled.boolValue, GUILayout.Width(50f));
				GUILayout.Space(10f);
				EditorGUILayout.EndHorizontal();
			});
		}

		private static void SearchAssetScripts() {
			var classNames = new List<string>();
			foreach (var file in Directory.GetFiles(Application.dataPath,
				"*.cs",
				SearchOption.AllDirectories)) {
				var stringArray = File.ReadAllLines(file);
				foreach (var className in GetScriptsName(stringArray)) {
					if (!FindDebug(stringArray, className))
						continue;
					classNames.Add(className);
				}
			}

			classNames.Sort();

			foreach (var className in classNames) {
				if (_array.Find(f => f.FindPropertyRelative("Name").stringValue == className) != null)
					continue;
				++_array.arraySize;
				var arrayElementAtIndex = _array.GetArrayElementAtIndex(_array.arraySize - 1);
				arrayElementAtIndex.FindPropertyRelative("Name").stringValue = className;
				arrayElementAtIndex.FindPropertyRelative("Color").colorValue =
					Random.ColorHSV(0.0f, 1f, 0.0f, 1f, 0.0f, 1f, 1f, 1f);
				arrayElementAtIndex.FindPropertyRelative("UseColor").boolValue = true;
				arrayElementAtIndex.FindPropertyRelative("IsLogEnabled").boolValue = true;
			}

			var removed = new List<string>();
			_array.ForEach((property, index) => {
				var name = property.FindPropertyRelative("Name").stringValue;
				if (!classNames.Contains(name))
					removed.Add(name);
			});

			foreach (var remove in removed)
				_array.Remove(f => f.FindPropertyRelative("Name").stringValue == remove);
		}

		private static string[] GetScriptsName(string[] scriptLines) {
			var stringList = new List<string>();
			for (var index1 = 0; index1 < scriptLines.Length; ++index1) {
				var strArray1 = scriptLines[index1]
					.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
				for (var index2 = 0; index2 < strArray1.Length; ++index2)
					if (strArray1[index2] == "class" || strArray1[index2] == "struct") {
						if (strArray1.Length == index2 + 1) {
							var strArray2 = scriptLines[index1 + 1]
								.Replace('\t', char.MinValue)
								.Replace(' ', char.MinValue)
								.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries);
							stringList.Add(RemoveGenerics(strArray2[0]));
						}
						else {
							var strArray2 = strArray1[index2 + 1]
								.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries);
							stringList.Add(RemoveGenerics(strArray2[0]));
						}
					}
			}

			return stringList.ToArray();
		}

		private static string RemoveGenerics(string @class) {
			var startIndex = @class.IndexOf('<');
			return startIndex != -1 ? @class.Remove(startIndex) : @class;
		}
		
		private static bool FindDebug(string[] fileText, string @class) {
			for (var i = 0; i < fileText.Length; ++i) {
				var line = fileText[i];

				for (var j = 0; j < line.Length; j++) {
					var c = line[j];

					if (c != '[')
						continue;

					if (!FindEndCharInLine(line, j, out var endCharIndex))
						continue;

					j++;
					var content = line.Substring(j, endCharIndex - j);
					if (content.Contains(@class))
						return true;

					j = endCharIndex;
				}
			}

			return false;
		}

		private static bool FindEndCharInLine(string line, int startIndex, out int endIndex) {
			endIndex = -1;

			for (var i = startIndex; i < line.Length; i++) {
				var c = line[i];

				if (c != ']')
					continue;
				endIndex = i;
				return true;
			}

			return false;
		}
	}
}