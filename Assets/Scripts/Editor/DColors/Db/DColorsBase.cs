﻿using System.Linq;
using DColors.Models;
using UnityEngine;

namespace DColors.Db {
    [CreateAssetMenu(fileName = "DColorsBase", menuName = "Helpers/DColorsBase")]
    public class DColorsBase : ScriptableObject
    {
        [HideInInspector] [SerializeField] private bool _isLogOther = true;
        [HideInInspector] [SerializeField] private ClassColor[] _array = new ClassColor[0];

        public bool IsLogOther => _isLogOther;

        public ClassColor HasClass(string name)
        {
            if (_array == null || _array.Length == 0)
                return null;
            return _array.FirstOrDefault(f => f.Name == name);
        }
    }
}