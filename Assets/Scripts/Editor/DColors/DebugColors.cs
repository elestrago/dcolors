﻿using DColors.Db;
using UnityEditor;
using UnityEngine;

namespace DColors {
    [InitializeOnLoad]
    public sealed class DebugColors
    {
        static DebugColors()
        {
            if (!Application.isEditor)
                return;
            var classColors = Resources.Load<DColorsBase>("DColorsBase");
            if (classColors == null)
                return;
            var defaultLogHandler = Debug.unityLogger.logHandler;
            Debug.unityLogger.logHandler = new LogHandler(classColors, defaultLogHandler);
        }
    }
}