﻿using System;
using UnityEngine;

namespace DColors.Models
{
    [Serializable]
    public class ClassColor
    {
        public string Name;
        public Color Color;
        public bool UseColor;
        public bool IsLogEnabled;

        public ClassColor(string name, Color color, bool useColor)
        {
            Name = name;
            Color = color;
            UseColor = useColor;
        }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(Name);
        }
    }
}