﻿using System;
using System.Runtime.CompilerServices;
using DColors.Db;
using DColors.Extensions;
using DColors.Models;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DColors {
	public class LogHandler : ILogHandler {
		private readonly ILogHandler _defaultLogger;
		private readonly DColorsBase _dColors;

		public LogHandler(DColorsBase dColors, ILogHandler defaultLogger) {
			_dColors = dColors;
			_defaultLogger = defaultLogger;
		}

		public void LogFormat(LogType logType, Object context, string format, params object[] args) {
			var logText = string.Format(format, args);
			var classColor = GetClassColor(logText);
			if (classColor == null)
				LogSimple(logType, context, format, args);
			else
				LogColor(logType, context, classColor, logText);
		}

		private void LogSimple(LogType logType, Object context, string format, params object[] args) {
			if (!_dColors.IsLogOther && !IsCriticalLog(logType))
				return;
			_defaultLogger.LogFormat(logType, context, format, args);
		}

		private void LogColor(LogType logType, Object context, ClassColor classColor, string logText) {
			const string format = "{0}";
			if (!classColor.IsLogEnabled && !IsCriticalLog(logType))
				return;

			if (!classColor.UseColor) {
				LogSimple(logType, context, format, logText);
				return;
			}

			var oldText = "[" + classColor.Name + "]";
			var newText = $"<b><color={classColor.Color.ToRgbaHex()}>[{classColor.Name}]</color></b>";
			logText = logText.Replace(oldText, newText);
			_defaultLogger.LogFormat(logType, context, format, logText);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private ClassColor GetClassColor(string logText) {
			var indexStart = logText.IndexOf('[') + 1;
			var indexEnd = logText.IndexOf(']');
			if (indexStart == 0 || indexEnd == -1)
				return null;
			var destination = new char[indexEnd - indexStart];
			logText.CopyTo(indexStart, destination, 0, destination.Length);
			return _dColors.HasClass(new string(destination));
		}

		public void LogException(Exception exception, Object context) {
			_defaultLogger.LogException(exception, context);
		}

		private static bool IsCriticalLog(LogType type)
			=> type == LogType.Error || type == LogType.Assert || type == LogType.Exception;
	}
}