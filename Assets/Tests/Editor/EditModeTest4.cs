﻿using System;
using NUnit.Framework;
using UnityEngine;

namespace Tests.Editor {
	public class EditModeTest4 {

		[Test]
		public void EditModeTest4SimplePasses() {
			// Use the Assert class to test conditions.
			Debug.Log($"[EditModeTest4] Test");
			Debug.LogAssertion($"[EditModeTest4] Test");
			Debug.LogError($"[EditModeTest4] Test");
			Debug.LogException(new Exception("[EditModeTest4] Test"));
			Debug.Log("Test");
		}
	}
}
