using NUnit.Framework;
using UnityEngine;

namespace Tests.Editor {
	public class TraceJsonLogTest {

		[Test]
		public void TraceJson_Success() {
			// Use the Assert class to test conditions.
			var json = Resources.Load<TextAsset>("json");
			UnityEngine.Debug.Log($"[{nameof(TraceJsonLogTest)}] {json.text}");
		}
		
	}
}