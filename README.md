This plugin simple at use and help you control logs. 

Features:
 - Disable all logs at not registered classes (logging only Exceptions)
 - Customuse class name color at console
 - Enable logs only at classes what you need
 - Disable all logs (logging only Exceptions)

How use this:
1. For use this plugin, you need create Dll file from path ...\Assets\Scripts\Editor\DColors
2. Copy this Dll to project and set IncludePlatforms -> Editor
3. Create Editor\Resources folder and in this resources file add DColorsBase from Create -> Helpers -> DColorsBase

Add log in class `Debug.Log("[ClassName] Your log text.");`, after assembly compiled 
DColors automaticly refresh all classes where has TAG and add this class to base.

WARNING: Class name and file name must match.

